package com.example.employeeintent

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class EmployeeAdapter(private val employees: ArrayList<Employee>,var itemClick:EmployeeListener) : RecyclerView.Adapter<EmployeeVH>() {

    interface EmployeeListener{
        fun onEmployeeSelected(employee: Employee)
        fun onEmployeeDeleted(employee: Employee)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeVH {

        return EmployeeVH(
            LayoutInflater
                .from(parent.context)
                .inflate(
                    R.layout.item_list,
                    parent,
                    false
                ),
            itemClick
        )
    }

    override fun getItemCount(): Int = employees.size

    override fun onBindViewHolder(holder: EmployeeVH, position: Int) {

        holder.bindEmployee(employees[position])

    }
}