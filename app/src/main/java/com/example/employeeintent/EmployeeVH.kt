package com.example.employeeintent

import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EmployeeVH(itemView: View, var itemClick: EmployeeAdapter.EmployeeListener) : RecyclerView.ViewHolder(itemView) {

    companion object {
        val TAG = EmployeeVH::class.java.simpleName
    }

    private val mIdTV = itemView.findViewById<TextView>(R.id.idTV)
    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mAgeTV = itemView.findViewById<TextView>(R.id.ageTV)
    private val mSalaryTv = itemView.findViewById<TextView>(R.id.salaryTV)
    private val mRemoveBTN = itemView.findViewById<ImageButton>(R.id.removeBTN)

    fun bindEmployee(employee: Employee) {
        mIdTV.text = employee.id
        mNameTV.text = employee.name
        mAgeTV.text = employee.age
        mSalaryTv.text = employee.salary

        itemView.setOnClickListener {
            itemClick.onEmployeeSelected(employee)
        }

        mRemoveBTN.setOnClickListener {
            itemClick.onEmployeeDeleted(employee)
        }
    }
}