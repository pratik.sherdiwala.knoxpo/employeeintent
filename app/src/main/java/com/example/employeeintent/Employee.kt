package com.example.employeeintent

import com.google.gson.annotations.SerializedName

class Employee {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("employee_name")
    var name: String? = null

    @SerializedName("employee_salary")
    var salary: String? = null

    @SerializedName("employee_age")
    var age: String? = null

    @SerializedName("profile_image")
    var profileImage: String? = null

}