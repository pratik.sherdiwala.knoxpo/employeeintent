package com.example.employeeintent

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiInterface{

    @GET("employees")
    fun getAllEmployees():Call<ArrayList<Employee>>

    @PUT("update/{id}")
    fun updateEmployee(@Path("id") id: String?,@Body request: EmployeeRequest):Call<EmployeeRequest>

    @PUT("create")
    fun createEmployee(@Body request: EmployeeRequest):Call<Employee>

    @GET("delete/{id}")
    fun deleteEmployee(@Path("id") id:String?):Call<DeleteResponse>

    @GET("employee/{id}")
    fun getEmployee(@Path("id")id:String):Call<Employee>

}