package com.example.employeeintent

import com.google.gson.annotations.SerializedName

class EmployeeRequest {

    @SerializedName("name")
    var name:String?=null

    @SerializedName("salary")
    var salary:String?=null

    @SerializedName("age")
    var age:String?=null

}