package com.example.employeeintent

import com.google.gson.annotations.SerializedName

class DeleteResponse {

    @SerializedName("success")
    var success: DeleteText? = null

}

class DeleteText {

    @SerializedName("text")
    var text:String?=null

}