package com.example.employeeintent

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), EmployeeAdapter.EmployeeListener {


    companion object {
        val TAG = MainActivity::class.java.simpleName
    }

    var employeesData: ArrayList<Employee> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        itemsRV.layoutManager = LinearLayoutManager(this)
        itemsRV.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        var apiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)

        apiInterface.getAllEmployees().enqueue(object : Callback<ArrayList<Employee>> {

            override fun onFailure(call: Call<ArrayList<Employee>>, t: Throwable) {
                Log.e(TAG, "onFailure()", t)
            }

            override fun onResponse(call: Call<ArrayList<Employee>>, response: Response<ArrayList<Employee>>) {
                employeesData = response.body()!!
                itemsRV.adapter = EmployeeAdapter(employeesData, this@MainActivity)
                itemsRV.adapter?.notifyDataSetChanged()
            }
        })
    }

    override fun onEmployeeSelected(employee: Employee) {
        startActivity(
            EmployeeEditActivity.intentForEditEmployee(this, employee.id!!)
        )
    }

    override fun onEmployeeDeleted(employee: Employee) {
        var apiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)

        apiInterface.deleteEmployee(employee.id).enqueue(object : Callback<DeleteResponse> {
            override fun onFailure(call: Call<DeleteResponse>, t: Throwable) {

                Log.e(EmployeeVH.TAG, "onFailure()", t)
            }

            override fun onResponse(call: Call<DeleteResponse>, response: Response<DeleteResponse>) {

                Log.e(EmployeeVH.TAG, "onResponse()" + response)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_employee, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?) = when (item?.itemId) {
        R.id.createBTN -> {
            startActivity(
                EmployeeEditActivity.intentForAddEmployee(this)
            )
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}