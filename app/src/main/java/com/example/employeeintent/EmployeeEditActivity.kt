package com.example.employeeintent

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_edit_activity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EmployeeEditActivity : AppCompatActivity() {

    companion object {
        private val TAG = EmployeeEditActivity::class.java.simpleName

        private const val ACTION_ADD = 0
        private const val ACTION_EDIT = 1

        private val EXTRA_EMPLOYEE_ID = "$TAG.EXTRA_EMPLOYEE_ID"
        private val EXTRA_ACTION = "$TAG.EXTRA_ACTION"

        fun intentForAddEmployee(context: Context): Intent {
            return Intent(context, EmployeeEditActivity::class.java).apply {
                putExtra(EXTRA_ACTION, ACTION_ADD)
            }
        }

        fun intentForEditEmployee(context: Context, employeeId: String): Intent {
            /*
            val intent = Intent(context, EmployeeEditActivity::class.java)
            intent.putExtra(EXTRA_EMPLOYEE_ID, employeeId)
            return intent
            */

            return Intent(context, EmployeeEditActivity::class.java).apply {
                putExtra(EXTRA_EMPLOYEE_ID, employeeId)
                putExtra(EXTRA_ACTION, ACTION_EDIT)
            }
        }
    }

    private val employeeRequest: EmployeeRequest
        get() {
            val request = EmployeeRequest()
            request.name = nameET.text.toString()
            request.age = ageET.text.toString()
            request.salary = salaryET.text.toString()
            return request
        }

    private val apiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_activity)

        when (intent.getIntExtra(EXTRA_ACTION, -1)) {
            ACTION_ADD -> {
                updateBTN.setOnClickListener {
                    addEmployee()
                }
                updateBTN.setText(R.string.action_add)
            }
            ACTION_EDIT -> {
                val employeeId = intent.getStringExtra(EXTRA_EMPLOYEE_ID)
                getEmployee(employeeId)
                updateBTN.setOnClickListener {
                    updateEmployee(employeeId)
                }
                updateBTN.setText(R.string.action_edit)
            }
        }
    }

    private fun getEmployee(employeeId: String) {
        apiInterface.getEmployee(employeeId).enqueue(object : Callback<Employee> {

            override fun onFailure(call: Call<Employee>, t: Throwable) {

                Log.e(TAG, "onError()", t)

            }

            override fun onResponse(call: Call<Employee>, response: Response<Employee>) {

                Log.e(TAG, "create onResponse()")
                idET.setText(intent.getStringExtra(EXTRA_EMPLOYEE_ID))
                nameET.setText(response.body()?.name)
                ageET.setText(response.body()?.age)
                salaryET.setText(response.body()?.salary)
            }
        })
    }

    private fun updateEmployee(employeeId: String) {

        apiInterface.updateEmployee(employeeId, employeeRequest).enqueue(object : Callback<EmployeeRequest> {

            override fun onFailure(call: Call<EmployeeRequest>, t: Throwable) {
                Log.e(TAG, "onFailure()", t)
            }

            override fun onResponse(call: Call<EmployeeRequest>, response: Response<EmployeeRequest>) {

                Toast.makeText(this@EmployeeEditActivity, "", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun addEmployee() {


        apiInterface.createEmployee(employeeRequest).enqueue(object : Callback<Employee> {
            override fun onFailure(call: Call<Employee>, t: Throwable) {

                Log.e(EmployeeEditActivity.TAG, "onFailure()", t)
            }

            override fun onResponse(call: Call<Employee>, response: Response<Employee>) {

                Toast.makeText(this@EmployeeEditActivity, "onResponse()", Toast.LENGTH_SHORT).show()
            }
        })
    }


}